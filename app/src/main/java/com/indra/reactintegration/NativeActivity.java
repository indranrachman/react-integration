package com.indra.reactintegration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

public class NativeActivity extends Activity implements View.OnClickListener {

    private Button openReactButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openReactButton = (Button) findViewById(R.id.button_open_react);
        openReactButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == openReactButton) {
            startActivity(new Intent(this, ReactActivity.class));
        }
    }
}
